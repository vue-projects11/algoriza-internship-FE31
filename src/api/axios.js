import axios from 'axios';
import { useUserStore } from '@/stores/userStore';

const rapidApiKey = '27d72c6c79mshbebe7064f4b3adfp18a41ajsnea42b700367d';

const instance = axios.create({
  baseURL: 'https://booking-com15.p.rapidapi.com/api/v1/',
  headers: {
    'X-RapidAPI-Key': rapidApiKey,
    'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com',
  },
});

instance.interceptors.request.use((config) => {
  const token = useUserStore.token;

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  return config;
});

export default instance;
